package fr.dorino.diag;

import fr.dorino.diag.lib.Assertion;
import fr.dorino.diag.lib.Check;
import fr.dorino.diag.lib.Checkpoint;
import fr.dorino.diag.lib.CheckpointType;
import fr.dorino.diag.lib.Choice;
import fr.dorino.diag.lib.Diagnostic;
import fr.dorino.diag.lib.DiagnosticException;
import fr.dorino.diag.lib.DiagnosticLib;
import fr.dorino.diag.lib.DiagnosticLibDatabase;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class DiagnosticLibDatabaseTest {

    private static DiagnosticLib lib = null;

    @BeforeClass
    public static void setUpClass() throws Exception {
        lib = new DiagnosticLibDatabase("data/diagtest.db");
        System.out.printf("\n***\n%s\n***\n\n", DiagnosticLib.Version.version());
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception  {
    lib.reset();
    }

    @After
    public void tearDown() throws Exception {
    lib.reset();
    }

    @Test
    public void checkVersion() throws Exception {
        System.out.println("Check version");
        assertTrue(lib.checkVersion());
    }

    @Test
    public void noDuplicateDiagnostic() throws Exception {
        System.out.println("No duplicate diagnostics");
        String name = "noDuplicateDiagnostic";
        int id = lib.addDiagnostic(name),
            idDup = lib.addDiagnostic(name);
        assertEquals(id, idDup);
    }

    private boolean searchDiagnostic(int id, String label) throws DiagnosticException {
        List<Diagnostic> diags = lib.getDiagnosticList(null);
        for (int i = 0; i < diags.size(); i++) {
            if (diags.get(i).id() == id && diags.get(i).label().equals(label)) {
                return true;
            }
        }
        return false;
    }

    @Test
    public void deletingDiagnostic() throws Exception {
        System.out.println("Deleting diagnostics");
        String name = "deletingDiagnostic";
        int id = lib.addDiagnostic(name);
        assertTrue(searchDiagnostic(id, name));
        assertTrue(lib.deleteDiagnostic(id));
        assertFalse(searchDiagnostic(id, name));
    }

    @Test
    public void noDuplicateCheck() throws Exception {
        System.out.println("No duplicate check");
        String name = "noDuplicateCheck";
        int id = lib.addCheck(name),
            idDup = lib.addCheck(name);
        assertEquals(id, idDup);
    }

    private boolean searchCheck(int id, String label) throws DiagnosticException {
        List<Check> f = lib.getCheckList(null);
        for (int i = 0; i < f.size(); i++) {
            if (f.get(i).id() == id && f.get(i).label().equals(label)) {
                return true;
            }
        }
        return false;
    }

    @Test
    public void updatingCheck() throws Exception {
        System.out.println("Updating check");
        String name = "deletingCheck";
        int id = lib.addCheck(name);
        assertTrue(searchCheck(id, name));
        assertTrue(lib.updateCheck(id, "changed"));
        assertTrue(searchCheck(id, "changed"));
        assertFalse(searchCheck(id, name));
    }

    @Test
    public void deletingCheck() throws Exception {
        System.out.println("Deleting check");
        String name = "deletingCheck";
        int id = lib.addCheck(name);
        assertTrue(searchCheck(id, name));
        assertTrue(lib.deleteCheck(id));
        assertFalse(searchCheck(id, name));
    }

    @Test
    public void checkAndDiagnostic() throws Exception {
        System.out.println("Check and diagnostic");
        String name = "checkAndDiagnostic";
        int checkId = lib.addCheck(name);
        int diagnosticId = lib.addDiagnostic(name);
        lib.connectCheckToDiagnostic(checkId, diagnosticId);
        assertTrue(lib.isConnectedCheckToDiagnostic(checkId, diagnosticId));
        lib.disconnectCheckFromDiagnostic(checkId, diagnosticId);
        assertFalse(lib.isConnectedCheckToDiagnostic(checkId, diagnosticId));
    }

    @Test
    public void gettingCheckAndDiagnostic() throws Exception {
        System.out.println("Getting check and diagnostic");
        String name = "gettingCheckAndDiagnostic";
        final int n = 10;
        int[] checkId = new int[n];
        int diagnosticId = lib.addDiagnostic(name);
        for (int i = 0; i < checkId.length; i++) {
            checkId[i] = lib.addCheck(name+i);
            if (i%2 == 0)
                lib.connectCheckToDiagnostic(checkId[i], diagnosticId);
        }
        //assertEquals(lib.getCheckList(null).size(), n);
        assertEquals(lib.getCheckList(null, diagnosticId).size(), n/2);
    }

    @Test
    public void noCheckPointWithoutCheck() throws Exception {
         System.out.println("No checkpoint without check");
         assertEquals(-1, lib.addCheckpoint(9999, CheckpointType.ASSERTION, "assertion"));
    }

    @Test
    public void assumeDuplicateCheckpoint() throws Exception {
        System.out.println("Assume duplicate checkpoint");
        int checkId = lib.addCheck("Family");
        int id = lib.addCheckpoint(checkId, CheckpointType.ASSERTION, "assertion"),
            idDup = lib.addCheckpoint(checkId, CheckpointType.ASSERTION, "assertion");
        assertNotEquals(id, idDup);
    }

    @Test
    public void assumeMultipeCheckpoint() throws Exception {
        System.out.println("Assume multiple checkpoint");
        int checkId = lib.addCheck("MultipleFamily");
        int id1 = lib.addCheckpoint(checkId, CheckpointType.ASSERTION, "assertion1"),
            id2 = lib.addCheckpoint(checkId, CheckpointType.ASSERTION, "assertion2"),
            id3 = lib.addCheckpoint(checkId, CheckpointType.ASSERTION, "assertion3");
        assertEquals(3, lib.getCheckpointList(null, checkId).size());
    }

    private boolean searchCheckpoint(int checkId, int id, String label) throws DiagnosticException {
        List<Checkpoint> c = lib.getCheckpointList(null, checkId);
        for (int i = 0; i < c.size(); i++) {
            if (c.get(i).id() == id && c.get(i).label().equals(label)) {
                return true;
            }
        }
        return false;
    }

    @Test
    public void deletingCheckpoint() throws Exception {
        System.out.println("Deleting checkpoint");
        int checkId = lib.addCheck("Check(Deleting checkpoint)");
        String assertion = "Assertion(Deleting checkpoint)";
        int id = lib.addCheckpoint(checkId, CheckpointType.ASSERTION, assertion);
        assertTrue(searchCheckpoint(checkId, id, assertion));
        lib.deleteCheckpoint(id);
        assertFalse(searchCheckpoint(checkId, id, assertion));
    }

    @Test
    public void updatingCheckpoint() throws Exception {
        System.out.println("Updating checkpoint");
        int checkId = lib.addCheck("Family(updating checkpoint)");
        String assertion = "Assertion(Updating checkpoint)";
        int id = lib.addCheckpoint(checkId, CheckpointType.ASSERTION, assertion);
        assertTrue(searchCheckpoint(checkId, id, assertion));
        assertTrue(lib.updateCheckpoint(id, CheckpointType.ASSERTION, assertion+" updated"));
        assertTrue(searchCheckpoint(checkId, id, assertion+" updated"));
    }

    @Test
    public void updatingWrongCheckpointChoice() throws Exception {
        System.out.println("Updating wrong checkpoint choice");
        int familyId = lib.addCheck("Check(updating wrong checkpoint choice)");
        String assertion = "Assertion(Updating wrong checkpoint choice)";
        int id = lib.addCheckpoint(familyId, CheckpointType.INT, assertion);
        assertFalse(lib.updateCheckpointSingleChoice(id, new ArrayList<>(), 0));
    }

    @Test
    public void updatingWrongCheckpointInt() throws Exception {
        System.out.println("Updating wrong checkpoint int");
        int checkId = lib.addCheck("Check(updating wrong checkpoint int)");
        String assertion = "Assertion(Updating wrong checkpoint choice)";
        int id = lib.addCheckpoint(checkId, CheckpointType.ASSERTION, assertion);
        assertFalse(lib.updateCheckpointInt(id, 0, 0, 10));
    }

    @Test
    public void updatingCheckpointChoice() throws Exception {
        System.out.println("Updating checkpoint choice");
        int checkId = lib.addCheck("Check(updating checkpoint choice)");
        String assertion = "Assertion(Updating checkpoint choice)";
        int id = lib.addCheckpoint(checkId, CheckpointType.SINGLE_CHOICE, assertion);
        assertTrue(searchCheckpoint(checkId, id, assertion));
        lib.updateCheckpointSingleChoice(id, new ArrayList<>(), 0);
        assertTrue(searchCheckpoint(checkId, id, assertion));
    }

    @Test
    public void updatingCheckpointInt() throws Exception {
        System.out.println("Updating checkpoint int");
        int checkId = lib.addCheck("Check(updating checkpoint int)");
        String assertion = "Assertion(Updating checkpoint int)";
        int id = lib.addCheckpoint(checkId, CheckpointType.INT, assertion);
        assertTrue(searchCheckpoint(checkId, id, assertion));
        lib.updateCheckpointInt(id, 0, 0, 10);
        assertTrue(searchCheckpoint(checkId, id, assertion));
    }

    @Test
    public void gettingCheckpoint() throws Exception {
        System.out.println("Getting checkpoint");
        int checkId = lib.addCheck("Check(getting checkpoint)");
        String assertion = "Assertion(Getting checkpoint)";
        int id = lib.addCheckpoint(checkId, CheckpointType.ASSERTION, assertion);
        assertTrue(searchCheckpoint(checkId, id, assertion));
        Checkpoint checkpoint = lib.getCheckpoint(id);
        assertTrue(checkpoint.label().equals(assertion));
    }

    @Test
    public void gettingNonExistentCheckpoint() throws Exception {
        System.out.println("Getting non-existent checkpoint");
        int checkId = lib.addCheck("Check(getting non-existent checkpoint)");
        assertEquals(null, lib.getCheckpoint(-1));
    }

    @Test
    public void checkpointWithQuotes() throws Exception {
        System.out.println("Checkpoint with quotes");
        int checkId = lib.addCheck("Family(quote)");
        String assertion1 = "Assertion\"double_quote", assertion2 = "assertion'single_quote";
        int id1 = lib.addCheckpoint(checkId, CheckpointType.ASSERTION, assertion1);
        assertTrue(searchCheckpoint(checkId, id1, assertion1));
        int id2 = lib.addCheckpoint(checkId, CheckpointType.ASSERTION, assertion2);
        assertTrue(searchCheckpoint(checkId, id2, assertion2));
    }

    @Test
    public void gettingAllCheckpoint() throws Exception {
        System.out.println("Getting all checkpoint");
        int checkId = lib.addCheck("Getting all checkpoints");
        lib.addCheckpoint(checkId, CheckpointType.ASSERTION, "Assertion1");
        lib.addCheckpoint(checkId, CheckpointType.ASSERTION, "Assertion2");
        lib.addCheckpoint(checkId, CheckpointType.INT, "Intervalle1");
        lib.addCheckpoint(checkId, CheckpointType.SINGLE_CHOICE, "Choix1");
        lib.addCheckpoint(checkId, CheckpointType.ASSERTION, "Assertion3");
        lib.addCheckpoint(checkId, CheckpointType.ASSERTION, "Assertion4");
        assertEquals(6, lib.getCheckpointList(null, checkId).size());
    }

    @Test
    public void updateCheckpointMandatory() throws Exception {
        System.out.println("Update checkpoint(mandatory)");
        int checkId = lib.addCheck("Update checkpoint mandatory");
        int checkpointId = lib.addCheckpoint(checkId, CheckpointType.ASSERTION, "Assertion1");
        lib.updateCheckpointAssertion(checkpointId, true);
        assertTrue(((Assertion)lib.getCheckpoint(checkpointId)).isMandatory());
    }

    @Test
    public void updateCheckpointDefaultChoice1() throws Exception {
        System.out.println("Update checkpoint(default choice) 1");
        int checkId = lib.addCheck("Update checkpoint default choice");
        int checkpointId = lib.addCheckpoint(checkId, CheckpointType.SINGLE_CHOICE, "Choix1");
        lib.updateCheckpointSingleChoice(checkpointId, new ArrayList<>(), -1);
        assertEquals(-1, ((Choice)lib.getCheckpoint(checkpointId)).defaultChoice());
    }

    @Test
    public void updateCheckpointDefaultChoice2() throws Exception {
        System.out.println("Update checkpoint(default choice) 2");
        int checkId = lib.addCheck("Update checkpoint default choice");
        int checkpointId = lib.addCheckpoint(checkId, CheckpointType.SINGLE_CHOICE, "Choix1");
        List<String> data = new ArrayList<>();
        data.add("Possible1");
        data.add("Possible2");
        lib.updateCheckpointSingleChoice(checkpointId, data, 1);
        assertEquals(1, ((Choice)lib.getCheckpoint(checkpointId)).defaultChoice());
    }


}
