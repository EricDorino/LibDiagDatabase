
drop table checkpoints;
drop table checkpoint_integer;
drop table checkpoint_choice;
drop table checks;
drop table diagnostics;
drop table diagnostic_check;
drop table version;

create table checkpoints (
    id integer primary key autoincrement,
    label varchar(4096) not null,
    "type" character(1) not null,   /* "Assertion", "Integer",
                                       "Single choice", "Multiple choice" */
    mandatory boolean,
    check_id integer not null,      /* the check this checkpoint belongs to */
    rank integer                    /* the rank of this checkpoint in the check */
);

create table checkpoint_integer (   /* Integer */
    id integer not null,            /* Must match checkpoint.id */
    "default", minimum, maximum integer
);

create table checkpoint_choice (    /* Single & Multiple choice */
    id integer not null,            /* Must match checkpoint.id */
    label varchar(1024),
    "default" boolean               /* Default label */
);

create table checks (
    id integer primary key autoincrement,
    label varchar(2048) not null
);

create table diagnostics (
    id integer primary key autoincrement,
    label varchar(2048) not null
);

create table diagnostic_check (
    diagnostic_id integer not null, /* the diagnostic */
    check_id integer not null,      /* the check that belongs to the diagnostic */
    rank integer                    /* the rank of the check in this diagnostic */
);

create table version (
    generation integer not null,
    release integer not null
);

insert into version(generation,release) values (0, 2);
