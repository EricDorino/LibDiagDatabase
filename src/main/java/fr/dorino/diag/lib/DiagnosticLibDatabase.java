
package fr.dorino.diag.lib;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class DiagnosticLibDatabase implements DiagnosticLib {

    private Connection connection = null;

    public DiagnosticLibDatabase(String databaseName) throws DiagnosticException {
        Utils.LOGGER.config(String.format("%s database \"%s\"\n", Version.version(), databaseName));
        if (databaseName == null)
            throw new DiagnosticException("No database specified");
        try {
            Class.forName("org.sqlite.JDBC");
            File file = new File(databaseName);
            if (!file.exists()) {
                Utils.LOGGER.severe(String.format("%s: %s: file not found\n", Version.LIBRARY, databaseName));
                throw new DiagnosticException(String.format("%s: File not found", databaseName));
            }
            connection = DriverManager.getConnection("jdbc:sqlite:" + databaseName);
            if (!checkVersion())
                throw new DiagnosticException(String.format("%s: database version mismatch", databaseName));
        } catch (ClassNotFoundException e) {
            Utils.LOGGER.severe(String.format("%s: No driver SQLite found: %s\n", Version.LIBRARY, e.getMessage()));
            throw new DiagnosticException("No driver SQLite found");
        } catch (SQLException e) {
            Utils.LOGGER.severe(String.format("SQL: %s\n", e.getMessage()));
            throw new DiagnosticException(e.getMessage());
        }
    }

    @Override
    public void finalize() throws SQLException, Throwable {
        try {
            if (connection != null) {
                connection.close();
                connection = null;
            }
        } finally {
            super.finalize();
        }
    }

    @Override
    public final boolean checkVersion() {
        try {
            boolean result = false;
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select generation,release from version");
            if (rs.next()) {
                result = Version.check(rs.getInt("generation"), rs.getInt("release"));
            }
            rs.close();
            stmt.close();
            return result;
        } catch (SQLException e) {
            Utils.LOGGER.severe(String.format("Wrong database format"));
            return false;
        }
    }

    private String normalize(String str) {
        return str.trim().replaceAll("\"", "\\\"").replaceAll("'", "''");
    }

    @Override
    public int addDiagnostic(String label) throws DiagnosticException {
        try {
            int result = -1;
            String select = "select id from diagnostics where label = '%s'",
                   insert = "insert into diagnostics (label) values ('%s')";
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(String.format(select, normalize(label)));
            if (!rs.next()) {
                stmt.executeUpdate(String.format(insert, normalize(label)));
                rs = stmt.executeQuery(String.format(select, normalize(label)));
                if (rs.next())
                    result = rs.getInt("id");
            }
            else
                result = rs.getInt("id");
            rs.close();
            stmt.close();
            return result;
        } catch (SQLException e) {
            throw new DiagnosticException(e);
        }
    }

    @Override
    public List<Diagnostic> getDiagnosticList(List<Diagnostic> list) throws DiagnosticException {
        try {
            List<Diagnostic> result = list == null ? new ArrayList<>() : list;
            result.clear();
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select id,label from diagnostics");
            while (rs.next()) {
                result.add(new Diagnostic(rs.getInt("id"), rs.getString("label")));
            }
            rs.close();
            stmt.close();
            Collections.sort(result);
            return result;
        } catch (SQLException e) {
            throw new DiagnosticException(e);
        }
    }

    @Override
    public boolean deleteDiagnostic(int id) throws DiagnosticException {
        try {
            String delete1 = "delete from diagnostics where id = '%d'",
                   delete2 = "delete from diagnostic_check where diagnostic_id = '%d'";
            connection.setAutoCommit(false);
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(String.format(delete1, id));
            stmt.executeUpdate(String.format(delete2, id));
            stmt.close();
            connection.commit();
            connection.setAutoCommit(true);
            return true;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException other) {
                throw new DiagnosticException(other);
            }
            throw new DiagnosticException(e);
        }
    }

    @Override
    public Check findCheck(int id) throws DiagnosticException {
        try {
            Check result = null;
            String select = "select label from checks where id = '%d'";
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(String.format(select, id));
            if (rs.next())
                result = new Check(id, rs.getString("label"));
            rs.close();
            stmt.close();
            return result;
        } catch (SQLException e) {
            throw new DiagnosticException(e);
        }
    }

    @Override
    public int findCheck(String label) throws DiagnosticException {
        try {
            int result = -1;
            String select = "select id from checks where label = '%s'";
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(String.format(select, normalize(label)));
            if (rs.next())
                result = rs.getInt("id");
            rs.close();
            stmt.close();
            return result;
        } catch (SQLException e) {
            throw new DiagnosticException(e);
        }
    }

    @Override
    public int addCheck(String label) throws DiagnosticException {
        try {
            int result = findCheck(label);
            String insert = "insert into checks (label) values ('%s')";
            Statement stmt = connection.createStatement();
            if (result < 0) {
                stmt.executeUpdate(String.format(insert, normalize(label)));
                result = findCheck(label);
            }
            stmt.close();
            return result;
        } catch (SQLException e) {
            throw new DiagnosticException(e);
        }
    }

    @Override
    public List<Check> getCheckList(List<Check> list) throws DiagnosticException {
        try {
            List<Check> result = list == null ? new ArrayList<>() : list;
            result.clear();
            Statement stmt = connection.createStatement();

            ResultSet rs = stmt.executeQuery("select id,label from checks");
            while (rs.next()) {
                result.add(new Check(rs.getInt("id"), rs.getString("label")));
            }
            Collections.sort(result);
            return result;
        } catch (SQLException e) {
            throw new DiagnosticException(e);
        }
    }

    @Override
    public List<Check> getCheckList(List<Check> list, int diagnosticId) throws DiagnosticException {
        return getCheckList(list, diagnosticId, false);
    }

    @Override
    public List<Check> getCheckList(List<Check> list, int diagnosticId, boolean sortByRank) throws DiagnosticException {
        try {
            List<Check> result = list == null ? new ArrayList<>() : list;
            result.clear();
            Statement stmt = connection.createStatement();

            ResultSet rs = stmt.executeQuery(String.format(
                "select id,label,rank from checks,diagnostic_check where id=check_id and diagnostic_id='%s'",
                 diagnosticId));
            while (rs.next()) {
                result.add(new Check(rs.getInt("id"), rs.getString("label"), rs.getInt("rank")));
            }
            if (sortByRank)
                Collections.sort(result, (Check t1, Check t2) -> t1.rank()-t2.rank());
            else
                Collections.sort(result);
            return result;
        } catch (SQLException e) {
            throw new DiagnosticException(e);
        }
    }

    @Override
    public boolean updateCheck(int id, String label) throws DiagnosticException {
        try {
            String update = "update checks set label='%s' where id = '%d'";
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(String.format(update, normalize(label), id));
            stmt.close();
            return true;
        } catch (SQLException e) {
            throw new DiagnosticException(e);
        }
    }

    @Override
    public boolean deleteCheck(int id) throws DiagnosticException {
        try {
            String select = "select * from checkpoints where check_id = '%d'",
                   delete1 = "delete from checks where id = '%d'",
                   delete2 = "delete from diagnostic_check where diagnostic_id = '%d'";
            connection.setAutoCommit(false);
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(String.format(select, id));
            if (rs.next()) {
                stmt.close();
                return false;
            }
            stmt.executeUpdate(String.format(delete1, id));
            stmt.executeUpdate(String.format(delete2, id));
            stmt.close();
            connection.commit();
            connection.setAutoCommit(true);
            return true;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException other) {
                throw new DiagnosticException(other);
            }
            throw new DiagnosticException(e);
        }
    }

    @Override
    public boolean isConnectedCheckToDiagnostic(int checkId, int diagnosticId)
            throws DiagnosticException {
        try {
            boolean result = false;
            String select = "select * from diagnostic_check where check_id = '%d' and diagnostic_id = '%d'";
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(String.format(select, checkId, diagnosticId));
            if (rs.next())
                result = true;
            rs.close();
            stmt.close();
            return result;
        } catch (SQLException e) {
            throw new DiagnosticException(e);
        }
    }

    @Override
    public void connectCheckToDiagnostic(int checkId, int diagnosticId) throws DiagnosticException {
        try {
            if (isConnectedCheckToDiagnostic(checkId, diagnosticId))
                return;
            String insert = "insert into diagnostic_check (check_id,diagnostic_id) values ('%d','%d')";
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(String.format(insert, checkId, diagnosticId));
            stmt.close();
        } catch (SQLException e) {
            throw new DiagnosticException(e);
        }
    }

    @Override
    public void disconnectCheckFromDiagnostic(int checkId, int diagnosticId) throws DiagnosticException {
        try {
            String delete = "delete from diagnostic_check where check_id = '%d' and diagnostic_id = '%d'";
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(String.format(delete, checkId, diagnosticId));
            stmt.close();
        } catch (SQLException e) {
            throw new DiagnosticException(e);
        }
    }

    @Override
    public void updateDiagnosticCheckOrder(List<Check> list, int diagnosticId)
            throws DiagnosticException {
        try {
            String update = "update diagnostic_check set rank='%d' where diagnostic_id='%d' and check_id='%d'";
            Iterator<Check> iter = list.iterator();
            connection.setAutoCommit(false);
            try (Statement stmt = connection.createStatement()) {
                int rank = 0;
                while (iter.hasNext()) {
                    Check c = iter.next();
                    stmt.executeUpdate(String.format(update, rank++, diagnosticId, c.id()));
                }
            }
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new DiagnosticException(e);
        }
    }

    @Override
    public int addCheckpoint(int checkId, CheckpointType type, String label)
            throws DiagnosticException {
        try {
            int id = -1;
            String selectf = "select id from checks where id='%d'",
                   insert = "insert into checkpoints (check_id,type,label,mandatory,rank) " +
                            "values ('%d','%c','%s','%b','%d')",
                   selectc = "select id from checkpoints where check_id='%d' and label = '%s'";
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(String.format(selectf, checkId));
            if (!rs.next()) {
                Utils.LOGGER.warning(String.format("No check id#%d", checkId));
                return -1;
            }
            stmt.executeUpdate(String.format(insert, checkId, CheckpointType.charValue(type),
                    normalize(label), false, Integer.MAX_VALUE));
            rs = stmt.executeQuery(String.format(selectc, checkId, normalize(label)));
            while (rs.next()) {
                id = rs.getInt("id");
            }
            rs.close();
            stmt.close();
            return id;
        } catch (SQLException e) {
            throw new DiagnosticException(e);
        }
    }

    @Override
    public List<Checkpoint> getCheckpointList(List<Checkpoint> list, int checkId) throws DiagnosticException {
        try {
            Statement stmt = connection.createStatement();
            String select = "select type,id,label,mandatory,rank from checkpoints where check_id = '%d'",
                   selectInt = "select \"default\",minimum,maximum from checkpoint_integer where id = '%d'",
                   selectChoice = "select \"default\",label from checkpoint_choice where id = '%d'";
            List<Checkpoint> result = list == null ? new ArrayList<>() : list;
            result.clear();
            ResultSet rs = stmt.executeQuery(String.format(select, checkId));
            while (rs.next()) {
                int id = rs.getInt("id");
                String label = rs.getString("label");
                boolean isMandatory = rs.getString("mandatory").equals("true"); // LOL
                int rank = rs.getInt("rank");
                Checkpoint checkpoint;
                switch (rs.getString("type").charAt(0)) {
                    case 'A':
                    default:
                        checkpoint = CheckpointFactory.createAssertion(id, checkId, label, isMandatory, rank);
                        break;
                    case 'I': {
                        Statement stmt1 = connection.createStatement();
                        ResultSet rs1 = stmt1.executeQuery(String.format(selectInt, id));
                        if (rs1.next())
                            checkpoint = CheckpointFactory.createInt(id, checkId, label, rank,
                                    rs1.getInt("minimum"), rs1.getInt("maximum"), rs1.getInt("default"));
                        else
                            checkpoint = CheckpointFactory.createInt(id, checkId, label, rank,
                                    Integer.MIN_VALUE, Integer.MAX_VALUE, 0);
                        rs1.close();
                        break;
                    }
                    case 'S': {
                        Statement stmt1 = connection.createStatement();
                        ResultSet rs1 = stmt1.executeQuery(String.format(selectChoice, id));
                        List<String> choice = new ArrayList<>();
                        int defaultChoice = -1, i = 0;
                        while (rs1.next()) {
                            choice.add(rs1.getString("label"));
                            if (rs1.getString("default").equals("true"))
                                defaultChoice = i;
                            i++;
                        }
                        checkpoint = CheckpointFactory.createSingleChoice(id, checkId, label, rank,
                                choice, defaultChoice);
                        rs1.close();
                        stmt1.close();
                        break;
                    }
                    case 'M': {
                        Statement stmt1 = connection.createStatement();
                        ResultSet rs1 = stmt1.executeQuery(String.format(selectChoice, id));
                        List<String> choice = new ArrayList<>();
                        int defaultChoice = -1, i = 0;
                        while (rs1.next()) {
                            choice.add(rs1.getString("label"));
                            if (rs1.getString("default").equals("true"))
                                defaultChoice = i;
                            i++;
                        }
                        checkpoint = CheckpointFactory.createMultipleChoice(id, checkId, label, rank,
                                choice, defaultChoice);
                        rs1.close();
                        stmt1.close();
                        break;
                    }
                }
                result.add(checkpoint);
            }
            Collections.sort(result);
            rs.close();
            stmt.close();
            return result;
        } catch (SQLException e) {
            throw new DiagnosticException(e);
        }
    }

    @Override
    public Checkpoint getCheckpoint(int id) throws DiagnosticException {
        try {
            Statement stmt = connection.createStatement();
            String select = "select \"type\",check_id,label,mandatory,rank from checkpoints where id = '%d'",
                   selectInt = "select \"default\",minimum,maximum from checkpoint_integer where id = '%d'",
                   selectChoice = "select \"default\",label from checkpoint_choice where id = '%d'";
            ResultSet rs = stmt.executeQuery(String.format(select, id));
            Checkpoint checkpoint = null;
            if (rs.next()) {
                String label = rs.getString("label");
                boolean isMandatory = rs.getString("mandatory").equals("true"); // LOL
                int checkId = rs.getInt("check_id"),
                    rank = rs.getInt("rank");
                switch (rs.getString("type").charAt(0)) {
                    case 'A':
                    default:
                        checkpoint = CheckpointFactory.createAssertion(id, checkId, label, isMandatory, rank);
                        break;
                    case 'I': {
                        Statement stmt1 = connection.createStatement();
                        ResultSet rs1 = stmt1.executeQuery(String.format(selectInt, id));
                        if (rs1.next())
                            checkpoint = CheckpointFactory.createInt(id, checkId, label, rank,
                                    rs1.getInt("minimum"), rs1.getInt("maximum"), rs1.getInt("default"));
                        else
                            checkpoint = CheckpointFactory.createInt(id, checkId, label, rank,
                                    Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE);
                        rs1.close();
                        stmt1.close();
                        break;
                    }
                    case 'S': {
                        Statement stmt1 = connection.createStatement();
                        ResultSet rs1 = stmt1.executeQuery(String.format(selectChoice, id));
                        List<String> choice = new ArrayList<>();
                        int defaultChoice = -1, i = 0;
                        while (rs1.next()) {
                            choice.add(rs1.getString("label"));
                            if (rs1.getString("default").equals("true"))
                                defaultChoice = i;
                            i++;
                        }
                        checkpoint = CheckpointFactory.createSingleChoice(id, checkId, label, rank,
                                choice, defaultChoice);
                        rs1.close();
                        stmt1.close();
                        break;
                    }
                    case 'M': {
                        Statement stmt1 = connection.createStatement();
                        ResultSet rs1 = stmt1.executeQuery(String.format(selectChoice, id));
                        List<String> choice = new ArrayList<>();
                        int defaultChoice = -1, i = 0;
                        while (rs1.next()) {
                            choice.add(rs1.getString("label"));
                            if (rs1.getString("default").equals("true"))
                                defaultChoice = i;
                            i++;
                        }
                        checkpoint = CheckpointFactory.createMultipleChoice(id, checkId, label, rank,
                                choice, defaultChoice);
                        rs1.close();
                        stmt1.close();
                        break;
                    }
                }
            }
            rs.close();
            stmt.close();
            return checkpoint;
        } catch (SQLException e) {
            throw new DiagnosticException(e);
        }
    }

    private void deleteCheckpointExtra(int id) throws SQLException {
        String delete = "delete from checkpoint_%s where id = '%d'";
        String[] extra = { "choice", "integer" };

        Statement stmt = connection.createStatement();
        for (int i = 0; i < extra.length; i++)
            stmt.executeUpdate(String.format(delete, extra[i], id));
        stmt.close();
    }

    @Override
    public boolean deleteCheckpoint(int id) throws DiagnosticException {
        try {
            String delete = "delete from checkpoints where id = '%d'";
            connection.setAutoCommit(false);
            deleteCheckpointExtra(id);
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(String.format(delete, id));
            stmt.close();
            connection.commit();
            connection.setAutoCommit(true);
            return true;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException other) {
                throw new DiagnosticException(other);
            }
            throw new DiagnosticException(e);
        }
    }

    @Override
    public boolean updateCheckpoint(int id, CheckpointType type, String label)
            throws DiagnosticException {
        try {
            String update = "update checkpoints set type='%c',label='%s' "+
                    "where id='%d'";
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(String.format(update,
                    CheckpointType.charValue(type),
                    normalize(label), id));
            stmt.close();
            return true;
        } catch (SQLException e) {
            throw new DiagnosticException(e);
        }
    }

    @Override
    public boolean updateCheckpointAssertion(int id, boolean isMandatory)
            throws DiagnosticException {
        try {
            String update = "update checkpoints set mandatory='%b' "+
                    "where id='%d'";
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(String.format(update, isMandatory, id));
            stmt.close();
            return true;
        } catch (SQLException e) {
            throw new DiagnosticException(e);
        }
    }


    private boolean validateCheckpointType(int id, CheckpointType type) throws DiagnosticException {
        try {
            boolean result = false;
            String select = "select type from checkpoints where id = '%d'";
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(String.format(select, id));
            if (rs.next())
                result = CheckpointType.charValue(type) == rs.getString("type").charAt(0);
            stmt.close();
            return result;
        } catch (SQLException e) {
            throw new DiagnosticException(e);
        }
    }

    private boolean updateCheckpointChoice(int id, List<String> choice, int defaultChoice)
            throws DiagnosticException {
        try {
            String insert = "insert into checkpoint_choice (id,label,\"default\") " +
                                    "values ('%d','%s','%b')";
            connection.setAutoCommit(false);
            deleteCheckpointExtra(id);
            Statement stmt = connection.createStatement();
            if (defaultChoice > choice.size()-1)
                defaultChoice = 0;
            for (int i = 0; i < choice.size(); i++) {
                stmt.executeUpdate(String.format(insert, id, normalize(choice.get(i)), i == defaultChoice));
            }
            stmt.close();
            connection.commit();
            connection.setAutoCommit(true);
            return true;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException other) {
                throw new DiagnosticException(other);
            }
            throw new DiagnosticException(e);
        }
    }

    @Override
    public boolean updateCheckpointSingleChoice(int id, List<String> choice, int defaultChoice)
            throws DiagnosticException {
        if (!validateCheckpointType(id, CheckpointType.SINGLE_CHOICE)) {
            Utils.LOGGER.warning(String.format("No checkpoint id#%d found", id));
            return false;
        }
        return updateCheckpointChoice(id, choice, defaultChoice);
    }

    @Override
    public boolean updateCheckpointMultipleChoice(int id, List<String> choice, int defaultChoice)
            throws DiagnosticException {
        if (!validateCheckpointType(id, CheckpointType.MULTIPLE_CHOICE)) {
            Utils.LOGGER.warning(String.format("No checkpoint id#%d found", id));
            return false;
        }
        return updateCheckpointChoice(id, choice, defaultChoice);
    }

    @Override
    public boolean updateCheckpointInt(int id, int defaultValue, int minValue, int maxValue)
            throws DiagnosticException {
        try {
            if (!validateCheckpointType(id, CheckpointType.INT)) {
                Utils.LOGGER.warning(String.format("No checkpoint id#%d found", id));
                return false;
            }
            String insert = "insert into checkpoint_integer (id,\"default\",minimum,maximum) " +
                                    "values ('%d','%d','%d','%d')";
            connection.setAutoCommit(false);
            deleteCheckpointExtra(id);
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(String.format(insert, id, defaultValue, minValue, maxValue));
            stmt.close();
            connection.commit();
            connection.setAutoCommit(true);
            return true;
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException other) {
                throw new DiagnosticException(other);
            }
            throw new DiagnosticException(e);
        }
    }

    @Override
    public void updateCheckpointOrder(List<Checkpoint> list) throws DiagnosticException {
        try {
            String update = "update checkpoints set rank='%d' where id='%d'";
            Iterator<Checkpoint> iter = list.iterator();
            connection.setAutoCommit(false);
            Statement stmt = connection.createStatement();
            int rank = 0;
            while (iter.hasNext()) {
                Checkpoint cp = iter.next();
                stmt.executeUpdate(String.format(update, rank, cp.id()));
                cp.setRank(rank++);
            }
            stmt.close();
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new DiagnosticException(e);
        }
    }

    @Override
    public void reset() throws DiagnosticException {
        try {
            String[] commands = new String[]{
                "delete from checkpoints",
                "delete from checkpoint_integer",
                "delete from checkpoint_choice",
                "delete from checks",
                "delete from diagnostics",
                "delete from diagnostic_check"
            };
            Statement stmt = connection.createStatement();
            for (int i = 0; i < commands.length; i++)
                stmt.addBatch(commands[i]);
            stmt.executeBatch();
            stmt.close();
        } catch (SQLException e) {
            throw new DiagnosticException(e);
        }
    }

}
